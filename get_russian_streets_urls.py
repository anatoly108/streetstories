from SPARQLWrapper import SPARQLWrapper, JSON
sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
sparql.setQuery("""
prefix schema: <http://schema.org/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>

#added before 2016-10
#defaultView:Table
SELECT DISTINCT ?item ?country ?article WHERE {
  ?item wdt:P17 wd:Q159.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "ru". }
  ?item wdt:P31 wd:Q79007.
  OPTIONAL { ?item wdt:P17 ?country. }
  OPTIONAL {
      ?article schema:about ?item .
      ?article schema:inLanguage "ru" .
      FILTER (SUBSTR(str(?article), 1, 25) = "https://ru.wikipedia.org/")
  }
}
#LIMIT 20
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

with open("russian_streets.txt", "w+") as f:
    for result in results["results"]["bindings"]:
        if "article" in result:
            f.write(result["article"]["value"] + "\n")

