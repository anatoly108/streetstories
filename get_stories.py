import re
from traceback import format_exc

import pandas as pd
from bs4 import BeautifulSoup, Tag
from tqdm import tqdm

PEOPLE = "people"
SIGHTS = "sights"
HISTORY = "history"
DESCRIPTION = "description"

tqdm.pandas()

data = pd.read_csv("russian_streets_htmls.csv")


def add_if_exists(text_by_header, array, text):
    array.extend(text_by_header[text]) if text in text_by_header else None


def extract_russian_texts(text_by_header):
    description = []
    add_if_exists(text_by_header, description, "Описание")
    add_if_exists(text_by_header, description, "Интересные факты")

    history = []
    add_if_exists(text_by_header, history, "История")
    add_if_exists(text_by_header, history, "История и достопримечательности")
    add_if_exists(text_by_header, history, "История названия")
    add_if_exists(text_by_header, history, "Происхождение названия")
    add_if_exists(text_by_header, history, "История наименования")
    add_if_exists(text_by_header, history, "История переименований")

    sights = []
    add_if_exists(text_by_header, sights, "Примечательные здания и сооружения")
    add_if_exists(text_by_header, sights, "Здания и сооружения")
    add_if_exists(text_by_header, sights, "Важнейшие здания и учреждения")
    add_if_exists(text_by_header, sights, "Достопримечательности")
    add_if_exists(text_by_header, sights, "Здания")
    add_if_exists(text_by_header, sights, "Объекты")
    add_if_exists(text_by_header, sights, "Примечательные здания")
    add_if_exists(text_by_header, sights, "Памятники")
    add_if_exists(text_by_header, sights, "Общественно значимые объекты")
    add_if_exists(text_by_header, sights, "Архитектура")
    add_if_exists(text_by_header, sights, "Здания и достопримечательности")
    add_if_exists(text_by_header, sights, "Памятники архитектуры")

    people = []
    add_if_exists(text_by_header, people, "Известные жители")

    return {
        DESCRIPTION: description,
        HISTORY: history,
        SIGHTS: sights,
        PEOPLE: people
    }


def get_tags_until_next(start_tag, tag_names_until="h2"):
    if not isinstance(tag_names_until, list):
        tag_names_until = [tag_names_until]

    current_tags = []
    next_node = start_tag
    while True:
        next_node = next_node.nextSibling
        if next_node is None:
            break
        if isinstance(next_node, Tag):
            if next_node.name in tag_names_until:
                break
            current_tags.append(next_node)
    return current_tags


def apply(row):
    result_columns = ["ObjectName", "Type", "Story", "Url"]
    html = row["Html"]
    try:
        soup = BeautifulSoup(html, 'html.parser')
    except Exception:
        print(format_exc())
        return pd.DataFrame([], columns=result_columns)

    text_by_header = {}
    for header in soup.find_all('h2'):
        header_text = header.get_text()
        header_text_fixed = remove_brackets(header_text)
        text_by_header[header_text_fixed] = get_tags_until_next(header)

    result = extract_russian_texts(text_by_header)

    result_filtered = []
    for stories_type, stories_tags in result.items():
        # TODO: preserve quotes! otherwise a story is cut off
        # TODO: extract pictures
        stories = extract_stories(stories_type, stories_tags)
        for story in stories:
            if len(story) < 120:
                continue

            result_filtered.append([row["StreetName"], stories_type, story, row["Url"]])

    stories_df = pd.DataFrame(result_filtered, columns=result_columns)
    return stories_df


def extract_sights_stroies(stories_tags):
    list_tags_names = ["ul", "dl", "ol"]
    list_tags = [tag for tag in stories_tags if tag.name in list_tags_names]
    if len(list_tags) == 0:
        return get_paragraphs_text(stories_tags)

    stories = []
    for list_tag in list_tags:
        list_items = list_tag.find_all(["li", "dt"])

        if len(list_items) == 1:
            story = remove_brackets(list_items[0].get_text())
            story_other_tags = get_tags_until_next(list_tag, list_tags_names)
            paragraphs_text = "\n".join(get_paragraphs_text(story_other_tags))
            if len(paragraphs_text) < 130:
                continue
            story = "%s: %s" % (story, paragraphs_text)
            stories.append(story)
            continue

        # list_items length is >= 2
        for item in list_items:
            story = remove_brackets(item.get_text())
            stories.append(story)

    return stories


def extract_stories(stories_type, stories_tags):
    if stories_type == SIGHTS:
        return extract_sights_stroies(stories_tags)

    stories = get_paragraphs_text(stories_tags)

    return stories


def get_paragraphs_text(stories_tags):
    return [remove_brackets(tag.get_text()) for tag in stories_tags if tag.name == "p"]


def remove_brackets(header_text):
    """
    removes text like "[edit]"
    :param header_text:
    :return:
    """
    return re.sub(r'\[.+?\]', '', header_text)


all_stories_list = list(data.progress_apply(lambda row: apply(row), axis=1))
all_stories = pd.concat(all_stories_list).reset_index(drop=True)

all_stories.to_csv("russian_streets_stories.csv", index=False)
