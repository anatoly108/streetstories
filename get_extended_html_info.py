from traceback import format_exc

import pandas as pd
from bs4 import BeautifulSoup
from tqdm import tqdm

tqdm.pandas()

input_file = "russian_streets_htmls.csv"
output_file = "russian_streets_htmls_extended.csv"


def apply(row):
    html = row["Html"]
    row["lon"] = None
    row["lat"] = None

    try:
        soup = BeautifulSoup(html, 'html.parser')
    except Exception:
        print(format_exc())
        return row

    try:
        location_tag = soup.find("a", {"class": "mw-kartographer-maplink"})
        row["Lon"] = float(location_tag["data-lon"])
        row["Lat"] = float(location_tag["data-lat"])
    except Exception:
        print(format_exc())
        return row

    return row


data = pd.read_csv(input_file)
data_with_location = data.progress_apply(lambda row: apply(row), axis=1)
data_with_location.to_csv(output_file, index=False)
