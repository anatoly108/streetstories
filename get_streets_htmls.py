import requests
import pandas as pd
from bs4 import BeautifulSoup

input_file = "russian_streets.txt"
output_file = "russian_streets_htmls.csv"

with open(input_file, "r") as f:
    russian_streets = f.readlines()

columns = ["StreetName", "City", "Headers", "Url", "Html"]
result_list = []

for i, street_url in enumerate(russian_streets):
    print("%i of %i" % (i, len(russian_streets)))
    r = requests.get(street_url.strip())
    soup = BeautifulSoup(r.text, 'html.parser')
    try:
        infobox = pd.read_html(str(soup.find_all("table", {"class": "infobox vcard"})[0]))[0]
        city = infobox[infobox[0] == "Город"].iloc[0][1]
    except Exception as e:
        print(e)
        city = None

    try:
        street_name = soup.find("h1").text
    except Exception as e:
        print(e)
        street_name = None

    try:
        headers = ",".join([headline.text for headline in soup.find_all("span", {"class": "mw-headline"})])
    except Exception as e:
        print(e)
        headers = None

    result_list.append([street_name, city, headers, street_url, str(soup)])
    # if i == 100:
    #     break

result = pd.DataFrame(result_list, columns=columns)
result.to_csv(output_file, index=False)
